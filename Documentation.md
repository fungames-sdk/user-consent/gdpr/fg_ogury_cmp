# Ogury GDPR

## Remote Config Settings

GdprType :  1

## Integration Steps

1) **"Install"** or **"Upload"** FG Ogury GDPR plugin from the FunGames Integration Manager in Unity, or download it from here.

2) Follow the instructions in the **"Install External Plugin"** section to import Ogury SDK.

3) Click on the **"Prefabs and Settings"** button in the FunGames Integration Manager to fill up your scene with required components and create the Settings asset.

4) To finish your integration, follow the **Account and Settings** section.

## Install External Plugin

After importing the FG OguryGDPR module from the Integration Manager window, you will find the last compatible version of Ogury SDK in the _Assets > FunGames_Externals > GDPR_ folder. Double click on the .unitypackage file to install it.

When integrating Ogury Unity SDK in a project that also integrate Ogury adapter for Applovin MAX, plz delete the _Assets/Editor/OgurySdkDependencies.xml_ file from your project.

**Note that the versions of included external SDKs are the latest tested and approved by our development team. We recommend using these versions to avoid any type of discrepencies. If, for any reason, you already have a different version of the plugin installed in your project that you wish to keep, please advise our dev team.**


## Account and Settings

To integrate Ogury GDPR throught FunGames SDK you first need to register an application in the Ogury dashboard. Ask your Publisher to create your app on their dashboard.

For more informations please follow their <a href="https://ogury-ltd.gitbook.io/unity/" target="_blank">documentation guide</a>.

Once done, you will be able to setup the FGOgurySettings (_Assets > Resources > FunGames > FGOgurySettings_) and fill it with the credentials of your Ogury app.

<img src="_source/oguryGdpr.png" width="256" height="540" style="display: block; margin: 0 auto"/>

## Build specifications

When integrating Ogury Unity SDK in a project that also integrate Ogury adapter for Applovin MAX, plz delete the _Assets/Editor/OgurySdkDependencies.xml_ file from your project.

**On iOS, Ogury CMP will only work with OguryUnitySdk v2.1.0**.

When building your project for iOS, you might need to add OMSDK_Ogury.xcframework manually to your Xcode Workspace. To do so, select Unity-iPhone target in XCode, open _General_ section, and add it using the "+" button in _Frameworks, Libraries and Embedded Content_.

![](_source/ogury_iOSbuild.png)